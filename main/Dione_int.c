/* GPIO Example

   This example code is in the Public Domain (or CC0 licensed, at your option.)

   Unless required by applicable law or agreed to in writing, this
   software is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
   CONDITIONS OF ANY KIND, either express or implied.
*/
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>
#include <sys/time.h>

#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/queue.h"

#include "driver/gpio.h"
#include "dione_config.h"
#include "esp_log.h"
#include "mqtt.h"

extern esp_err_t dione_nvs_save_shake(void);
extern char gCID[16];
extern uint64_t gSeq;
extern uint64_t gShakeCount;
extern bool gMQTTConnected;
extern mqtt_client *g_mqtt_client;
extern char *TAG_DIONE;


//#define GPIO_INPUT_IO_0     DIONE_SHAKE_PIN
#define DIONE_GPIO_INPUT_PIN_SEL  (1 << DIONE_SHAKE_PIN)
#define ESP_INTR_FLAG_DEFAULT 0

static xQueueHandle g_dione_gpio_evt_queue = NULL;
static uint8_t g_shake_state = 0;

static void IRAM_ATTR gpio_isr_handler(void* arg)
{
    if (g_shake_state != 0) return;
    g_shake_state = 1;
    uint32_t gpio_num = (uint32_t) arg;
    gpio_isr_handler_remove(DIONE_SHAKE_PIN);
    xQueueSendFromISR(g_dione_gpio_evt_queue, &gpio_num, NULL);
}

#ifdef MQTT_SERVER_DIONE
static void dione_shake_task(void* arg)
{
    uint32_t io_num;
    while (true)
    {
        if (xQueueReceive(g_dione_gpio_evt_queue, &io_num, portMAX_DELAY))
        {
            gpio_set_level(LED_SHAKE, LED_OFF);
            printf("GPIO[%d] intr, val: %d\n", io_num, gpio_get_level(io_num));

            char strftime_buf[64];
            time_t now;
            struct tm timeinfo;
            time(&now);
            localtime_r(&now, &timeinfo);
            strftime(strftime_buf, sizeof(strftime_buf), "%c", &timeinfo);
            if (gMQTTConnected)
            {
                mqtt_publish(g_mqtt_client, "Dione/Shake", (char*)strftime_buf, strlen(strftime_buf), 0, 0);
            }
            
            vTaskDelay(3000 / portTICK_RATE_MS);

            gpio_set_level(LED_SHAKE, LED_ON);
            gpio_isr_handler_add(DIONE_SHAKE_PIN, gpio_isr_handler, (void*) DIONE_SHAKE_PIN);
            g_shake_state = 0;
        }
    }
}
#elif defined(MQTT_SERVER_ONENET)
static void dione_shake_task(void* arg)
{
    uint32_t io_num;
    while (true)
    {
        if (xQueueReceive(g_dione_gpio_evt_queue, &io_num, portMAX_DELAY))
        {
            gpio_set_level(LED_SHAKE, LED_OFF);
            printf("GPIO[%d] intr, val: %d\n", io_num, gpio_get_level(io_num));
            
            vTaskDelay(3000 / portTICK_RATE_MS);

            gpio_set_level(LED_SHAKE, LED_ON);
            gpio_isr_handler_add(DIONE_SHAKE_PIN, gpio_isr_handler, (void*) DIONE_SHAKE_PIN);
            g_shake_state = 0;
        }
    }
}
#elif defined(MQTT_SERVER_MICRODUINO)
static void dione_shake_task(void* arg)
{
    uint32_t io_num;
    while (true)
    {
        if (xQueueReceive(g_dione_gpio_evt_queue, &io_num, portMAX_DELAY))
        {
            gpio_set_level(LED_SHAKE, LED_OFF);
            printf("GPIO[%d] intr, val: %d\n", io_num, gpio_get_level(io_num));

            char strftime_buf[64];
            time_t now;
            struct tm timeinfo;
            time(&now);
            localtime_r(&now, &timeinfo);
            strftime(strftime_buf, sizeof(strftime_buf), "%c", &timeinfo);
            char *mqtt_buf = malloc(256);
            memset(mqtt_buf, 0, 256);
            sprintf(mqtt_buf, "{\"shake\":\"%s\"}", strftime_buf);
            if (gMQTTConnected)
            {
                mqtt_publish(g_mqtt_client, "dp/5936377a4eb6400001eb131a", mqtt_buf, strlen(mqtt_buf), 0, 0);
            }
            vTaskDelay(3000 / portTICK_RATE_MS);
            free(mqtt_buf);

            gpio_set_level(LED_SHAKE, LED_ON);
            gpio_isr_handler_add(DIONE_SHAKE_PIN, gpio_isr_handler, (void*) DIONE_SHAKE_PIN);
            g_shake_state = 0;
        }
    }
}
#elif defined(MQTT_SERVER_SH_HUOWEI)
static void dione_shake_task(void* arg)
{
    uint32_t io_num;
    while (true)
    {
        if (xQueueReceive(g_dione_gpio_evt_queue, &io_num, portMAX_DELAY))
        {
            gpio_set_level(LED_SHAKE, LED_OFF);
            printf("GPIO[%d] intr, val: %d\n", io_num, gpio_get_level(io_num));
            char *json_buf = malloc(1024);
            //sprintf(mqtt_buf, "{\"shake\":\"%s\"}", strftime_buf);
            if (gMQTTConnected)
            {
                sprintf(json_buf, "{\"cid\":\"%s\",\"seq\":%llu,\"time\":%lu,\"shake\":%llu}", 
                                      gCID, gSeq++, time(NULL), gShakeCount++);
                mqtt_publish(g_mqtt_client, MQTT_PUB_TOPIC_CONTROL, json_buf, strlen(json_buf), 0, 0);
                
            }
            vTaskDelay(3000 / portTICK_RATE_MS);
            free(json_buf);
            dione_nvs_save_shake();

            gpio_set_level(LED_SHAKE, LED_ON);
            gpio_isr_handler_add(DIONE_SHAKE_PIN, gpio_isr_handler, (void*) DIONE_SHAKE_PIN);
            g_shake_state = 0;
        }
    }
}
#endif

void dione_interrupt_init(void)
{
    ESP_LOGE(TAG_DIONE, "Enter %s", __func__);
    gpio_config_t io_conf;
    //interrupt of rising edge
    io_conf.intr_type = GPIO_PIN_INTR_POSEDGE;
    //bit mask of the pins, use GPIO4/5 here
    io_conf.pin_bit_mask = DIONE_GPIO_INPUT_PIN_SEL;
    //set as input mode    
    io_conf.mode = GPIO_MODE_INPUT;
    //enable pull-up mode
    io_conf.pull_up_en = 0;
    io_conf.pull_down_en = 1;
    gpio_config(&io_conf);

    //change gpio intrrupt type for one pin
    gpio_set_intr_type(DIONE_SHAKE_PIN, GPIO_INTR_POSEDGE);

    //create a queue to handle gpio event from isr
    g_dione_gpio_evt_queue = xQueueCreate(1, sizeof(uint32_t));
    //start gpio task
    xTaskCreate(dione_shake_task, "shake", 2048, NULL, 9, NULL);

    //install gpio isr service
    gpio_install_isr_service(ESP_INTR_FLAG_DEFAULT);
    //hook isr handler for specific gpio pin
    gpio_isr_handler_add(DIONE_SHAKE_PIN, gpio_isr_handler, (void*) DIONE_SHAKE_PIN);
}

