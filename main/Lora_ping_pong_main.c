/*
 * THE FOLLOWING FIRMWARE IS PROVIDED: (1) "AS IS" WITH NO WARRANTY; AND 
 * (2)TO ENABLE ACCESS TO CODING INFORMATION TO GUIDE AND FACILITATE CUSTOMER.
 * CONSEQUENTLY, SEMTECH SHALL NOT BE HELD LIABLE FOR ANY DIRECT, INDIRECT OR
 * CONSEQUENTIAL DAMAGES WITH RESPECT TO ANY CLAIMS ARISING FROM THE CONTENT
 * OF SUCH FIRMWARE AND/OR THE USE MADE BY CUSTOMERS OF THE CODING INFORMATION
 * CONTAINED HEREIN IN CONNECTION WITH THEIR PRODUCTS.
 * 
 * Copyright (C) SEMTECH S.A.
 */
/*! 
 * \file       main.c
 * \brief      Ping-Pong example application on how to use Semtech's Radio
 *             drivers.
 *
 * \version    2.0
 * \date       Nov 21 2012
 * \author     Miguel Luis
 */
#include <string.h>
#include <stdint.h>
#include <stdbool.h>
#include "driver/spi_master.h"
#include "driver/gpio.h"
#include "esp_system.h"
#include "platform.h"
#include "sx1276-Hal.h"
#include "sx1276-LoRa.h"
#include "sx1276.h"

//#include "led.h"
#if USE_UART
//#include "uart.h"
#endif
#include "radio.h"
#include "Dione_event.h"
#include "Dione_config.h"
#include "freertos/queue.h"
#include "esp_log.h"


static const char *TAG = "pingpong";
extern spi_device_handle_t spi_handle;
extern void dumpBytes(const uint8_t *data, size_t count);
extern int8_t rhea_SPI_Read1(uint8_t adr);
#define BUFFER_SIZE                                 9 // Define the payload size here

static uint16_t BufferSize = BUFFER_SIZE;			// RF buffer size
static uint8_t Buffer[BUFFER_SIZE];					// RF buffer

static uint8_t EnableMaster = true; 				// Master/Slave selection

tRadioDriver *Radio = NULL;

const uint8_t PingMsg[] = "PING";
const uint8_t PongMsg[] = "PONG";

extern xQueueHandle g_led_toggle_queue;
static void LedToggle(uint8_t led, uint8_t time)
{
    //xEventGroupSetBits(dione_event_group, LED_TOGGLE);
    xQueueSend(g_led_toggle_queue, &time, 0);
}

static void LedOff(uint8_t led)
{
    gpio_set_level(led, LED_OFF);
}

/*
 * Manages the master operation
 */
void OnMaster( void )
{
    uint8_t i;
    static uint8_t reg;
    static uint32_t cnt = 1;
    static uint32_t preprocessState = RF_IDLE;
    uint32_t processState = Radio->Process( );
    if (processState != preprocessState)
    {
        preprocessState = processState;
        ESP_LOGI(TAG, "Enter %s,processState=%d", __func__, processState);
    }
    if (cnt++ % 100 == 0)
    {
        //SX1276Read(REG_LR_OPMODE, &reg);
        SX1276Read(REG_LR_VERSION, &reg);
        //reg = rhea_SPI_Read1(REG_LR_VERSION);
        ESP_LOGD(TAG, "REG_LR_VERSION=0x%02X, freememory=%d", reg, esp_get_free_heap_size());
        
    }
    switch (processState)
    {
    case RF_RX_TIMEOUT:
        //ESP_LOGW(TAG, "RF_RX_TIMEOUT");
        // Send the next PING frame
        Buffer[0] = 'P';
        Buffer[1] = 'I';
        Buffer[2] = 'N';
        Buffer[3] = 'G';
        for( i = 4; i < BufferSize; i++ )
        {
            Buffer[i] = i - 4;
        }
        Radio->SetTxPacket( Buffer, BufferSize );
        break;
    case RF_RX_DONE:
        ESP_LOGE(TAG, "RF_RX_DONE");
        //ESP_LOGW(TAG, "RF_RX_DONE, rssi=%02f", SX1276LoRaGetPacketRssi());
        Radio->GetRxPacket( Buffer, ( uint16_t* )&BufferSize );
        dumpBytes(Buffer, BufferSize);
        if( BufferSize > 0 )
        {
            if( strncmp( ( const char* )Buffer, ( const char* )PongMsg, 4 ) == 0 )
            {
                // Indicates on a LED that the received frame is a PONG
                LedToggle( LED_BLE, 0 );

                // Send the next PING frame            
                Buffer[0] = 'P';
                Buffer[1] = 'I';
                Buffer[2] = 'N';
                Buffer[3] = 'G';
                // We fill the buffer with numbers for the payload 
                for( i = 4; i < BufferSize; i++ )
                {
                    Buffer[i] = i - 4;
                }
                Radio->SetTxPacket( Buffer, BufferSize );
            }
            else if( strncmp( ( const char* )Buffer, ( const char* )PingMsg, 4 ) == 0 )
            { // A master already exists then become a slave
                EnableMaster = false;
                LedOff( LED_UWB );
            }
        }            
        break;
    case RF_TX_DONE:
        //ESP_LOGW(TAG, "RF_TX_DONE");
        // Indicates on a LED that we have sent a PING
        LedToggle( LED_BLE, 1 );
        Radio->StartRx( );
        break;
    default:
        break;
    }
}

extern double RxPacketRssiValue;
/*
 * Manages the slave operation
 */
void OnSlave( void )
{
    static uint32_t cnt = 1;
    uint8_t i;
    uint8_t reg;
    static uint32_t preprocessState = RF_IDLE;
    if (cnt++ % 100 == 0)
    {
        //SX1276Read(REG_LR_OPMODE, &reg);
        SX1276Read(REG_LR_VERSION, &reg);
        //reg = rhea_SPI_Read1(REG_LR_VERSION);
        ESP_LOGD(TAG, "REG_LR_VERSION=0x%02X, freememory=%d", reg, esp_get_free_heap_size());
    }
    uint32_t processState = Radio->Process( );
    
    if (processState != preprocessState)
    {
        preprocessState = processState;
        ESP_LOGI(TAG, "Enter %s,processState=%d", __func__, processState);
    }
    //double rssi = RxPacketRssiValue;
    switch( processState )
    {
    case RF_RX_DONE:
        ESP_LOGE(TAG, "RF_RX_DONE");
        //ESP_LOGE(TAG, "RF_RX_DONE, RSSI=%02f", SX1276LoRaGetPacketRssi());
        //rssi = SX1276LoRaGetPacketRssi();
        //rssi = SX1276GetPacketRssi();
        Radio->GetRxPacket( Buffer, ( uint16_t* )&BufferSize );
        dumpBytes(Buffer, BufferSize);
        if( BufferSize > 0 )
        {
            if( strncmp( ( const char* )Buffer, ( const char* )PingMsg, 4 ) == 0 )
            {
                // Indicates on a LED that the received frame is a PING
                LedToggle( LED_BLE, 2 );

               // Send the reply to the PONG string
                Buffer[0] = 'P';
                Buffer[1] = 'O';
                Buffer[2] = 'N';
                Buffer[3] = 'G';
                // We fill the buffer with numbers for the payload 
                for( i = 4; i < BufferSize; i++ )
                {
                    Buffer[i] = i - 4;
                }

                Radio->SetTxPacket( Buffer, BufferSize );
            }
        }
        break;
    case RF_TX_DONE:
        ESP_LOGD(TAG, "RF_TX_DONE");
        // Indicates on a LED that we have sent a PONG
        LedToggle( LED_UWB, 3 );
        Radio->StartRx();
        break;
    case RF_RX_TIMEOUT:
        Radio->StartRx();
        break;
    default:
        break;
    }
}

static void ping_pong_task(void *pvParameter)
{
    uint8_t reg = 0x78;
    ESP_LOGW(TAG, "Enter %s", __func__);
    //xEventGroupWaitBits(dione_event_group, DIONE_EVENT_STA_GOT_IP, false, false, portMAX_DELAY);
#if 1
    esp_err_t ret;
	spi_bus_config_t buscfg;
	spi_device_interface_config_t devcfg;
	memset(&buscfg, 0, sizeof(buscfg));
	memset(&devcfg, 0, sizeof(devcfg));
	buscfg.miso_io_num = RHEA_LORA_MISO;
	buscfg.mosi_io_num = RHEA_LORA_MOSI;
	buscfg.sclk_io_num = RHEA_LORA_CLK;
	buscfg.quadwp_io_num = -1;
	buscfg.quadhd_io_num = -1;

	devcfg.clock_speed_hz = 1000000;
	devcfg.command_bits = 0;
	devcfg.mode = 0;
	devcfg.spics_io_num = RHEA_LORA_SS;
	devcfg.queue_size = 8;
    // Initialize the SPI bus
    ret = spi_bus_initialize(HSPI_HOST, &buscfg, 1);
    if (ret == ESP_OK)
    {
		ESP_LOGI(TAG, "spi_bus_initialize sucess.");
    }
	else
	{
		ESP_LOGE(TAG, "spi_bus_initialize fail.");
	}
    // Attach the SX1278/1276 to the SPI bus
    ret = spi_bus_add_device(HSPI_HOST, &devcfg, &spi_handle);
    if (ret == ESP_OK)
    {
		ESP_LOGI(TAG, "spi_bus_add_device sucess.");
    }
	else
	{
		ESP_LOGI(TAG, "spi_bus_add_device fail.");
	}
    gpio_pad_select_gpio(RHEA_LORA_DIO0);
    gpio_set_direction(RHEA_LORA_DIO0, GPIO_MODE_INPUT);
#endif

    #if 1
    vTaskDelay(3100 / portTICK_RATE_MS);
    gpio_set_level(RHEA_LORA_RESET, 1);
    vTaskDelay(10 / portTICK_RATE_MS);
    gpio_set_level(RHEA_LORA_RESET, 0);
	vTaskDelay(10 / portTICK_RATE_MS);
	gpio_set_level(RHEA_LORA_RESET, 1);
    vTaskDelay(100 / portTICK_RATE_MS);
    SX1276Read(REG_LR_VERSION, &reg);
    //reg = rhea_SPI_Read1(REG_LR_VERSION);
    ESP_LOGD(TAG, "REG_LR_VERSION=0x%02X", reg);
    Radio = RadioDriverInit( );
    Radio->Init( );
    Radio->StartRx( );
    while( 1 )
    {
        vTaskDelay(50 / portTICK_RATE_MS);
        if( EnableMaster == true )
        {
            OnMaster( );
        }
        else
        {
            OnSlave( );
        }
        //vTaskDelay(50 / portTICK_RATE_MS);
    }
    #else
    vTaskDelay(4100 / portTICK_RATE_MS);
    gpio_set_level(RHEA_LORA_RESET, 1);
    vTaskDelay(10 / portTICK_RATE_MS);
    gpio_set_level(RHEA_LORA_RESET, 0);
	vTaskDelay(1 / portTICK_RATE_MS);
	gpio_set_level(RHEA_LORA_RESET, 1);
    vTaskDelay(10 / portTICK_RATE_MS);
    SX1276InitIo();
    vTaskDelay(100 / portTICK_RATE_MS);
    while (1)
    {
        uint8_t reg = 0x76;
        #if 1
		SX1276Read(REG_LR_OPMODE, &reg);
		ESP_LOGW(TAG, "reg[%X] = 0x%X", REG_LR_OPMODE, reg);
		SX1276Read(REG_LR_VERSION, &reg);
		ESP_LOGW(TAG, "reg[%X] = 0x%X", REG_LR_VERSION, reg);
        #endif
        #if 0
        reg = rhea_SPI_Read1(REG_LR_VERSION);
        ESP_LOGW(TAG, "reg[%X] = 0x%X", REG_LR_VERSION, reg);
        #endif
        vTaskDelay(5000 / portTICK_RATE_MS);
    }
    #endif
}

/*
 * Main application entry point.
 */
void lora_ping_pong_main( void )
{
    ESP_LOGW(TAG, "Enter %s", __func__);
    //BoardInit( );
    //gpio_set_level(RHEA_LORA_RESET, 1);
    xTaskCreate(ping_pong_task, "PingPong", 4096, NULL, 23, NULL);
}
