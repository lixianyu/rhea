#ifndef __DIONE_CONFIG_LOCAL_H__
#define __DIONE_CONFIG_LOCAL_H__

#ifdef __cplusplus
extern "C"
{
#endif

#define DIONE_SW_VERSION "0.0.B05.00"

//#define BLE_LED_IN_CALLBACK

//#define MQTT_SERVER_DIONE
//#define MQTT_SERVER_ONENET
//#define MQTT_SERVER_MICRODUINO
#define MQTT_SERVER_SH_HUOWEI

//#define RHEA_CLOSE_WIFI
#define RHEA_CLOSE_ETHERNET

#define WIFI_SSID "TaiYangGong2.4G"
#define WIFI_PASS "6hy5BIktT"
//#define WIFI_PASS "9hy5BIktT"
//#define CPU_FREQ_160MHZ

#define LED_ON    1
#define LED_OFF   0


#define UART_BUF_SIZE (1024)
// For UART (UWB)
#define DIONE_UART_TXD (GPIO_NUM_16) // Not used in Rhea project.
#define DIONE_UART_RXD (GPIO_NUM_17) // Not used in Rhea project.


// For Lora gpio.
#define RHEA_LORA_MISO (GPIO_NUM_34) // VDET_1
#define RHEA_LORA_MOSI (GPIO_NUM_17)
#define RHEA_LORA_CLK  (GPIO_NUM_14) // MTMS
#define RHEA_LORA_SS   (GPIO_NUM_33) // 32K_XN

#define RHEA_LORA_RESET (GPIO_NUM_16)

#define RHEA_LORA_DIO0 GPIO_NUM_35
#define RHEA_LORA_DIO1 GPIO_NUM_37
#define RHEA_LORA_DIO2 GPIO_NUM_38
#define RHEA_LORA_DIO3 GPIO_NUM_39
#if 0
#define RHEA_LORA_DIO4 RHEA_LORA_DIO0
#define RHEA_LORA_DIO5 RHEA_LORA_DIO0
#else
#define RHEA_LORA_DIO4 GPIO_NUM_19
#define RHEA_LORA_DIO5 GPIO_NUM_27
#endif


// For trace
/*
 TXD0 to GPIO1
 RXD0 to GPIO3
 */

// For LEDs
#define LED_POWER GPIO_NUM_15 // Red, MTDO
#define LED_UWB   GPIO_NUM_5  // Green
#define LED_WIFI  GPIO_NUM_12 // Yellow, MTDI
#if 0
// The White board
#define LED_BLE   GPIO_NUM_25  // Blue
#else
// The black board
#define LED_BLE   GPIO_NUM_2  // Blue
#endif
#define LED_SHAKE LED_POWER

// For Ethernet
#define PIN_SMI_MDC   GPIO_NUM_23
#define PIN_SMI_MDIO  GPIO_NUM_18
/* phy_rmii_configure_data_interface_pins
   CRS_DRV to GPIO27
   TXD0 to GPIO19
   TX_EN to GPIO21
   TXD1 to GPIO22
   RXD0 to GPIO25
   RXD1 to GPIO26
   RMII CLK to GPIO0
 */

// For SPI Flash
/*
 SPIHD to GPIO9
 SPIWP to GPIO10
 SPICS0 to GPIO11
 SPICLK to GPIO6
 SPIQ to GPIO7
 SPID to GPIO8
 */

// For I2C
#define DIONE_I2C_SCL GPIO_NUM_4
#define DIONE_I2C_SDA GPIO_NUM_13 // MTCK


#define DIONE_POWER_ADC GPIO_NUM_35 // VDET_2

#define DIONE_SHAKE_PIN GPIO_NUM_14 // Not used in Rhea project.

#define VOLTAGE_5V_ADC_VALUE 3419


#define CONFIG_PHY_LAN8720
#define CONFIG_PHY_ADDRESS 1
#define CONFIG_PHY_USE_POWER_PIN

#ifdef MQTT_SERVER_SH_HUOWEI
#define MQTT_PUB_TOPIC_CONTROL "/queue/dione.up"
#define MQTT_PUB_TOPIC_DATA "/queue/dione.ibeacon"
#define MQTT_PUB_TOPIC_UWB "/queue/uwb"
#define MQTT_SUB_TOPIC "/topic/dione.down"
#else
#define MQTT_PUB_TOPIC ""
#define MQTT_SUB_TOPIC "Dione/command"
#endif

#define MQTT_UP_MSG_TYPE_0 0x0000 // 设备登录与心跳
#define MQTT_UP_MSG_TYPE_1 0x0001 // 设备基本配置信息上报
#define MQTT_UP_MSG_TYPE_2 0x0002 // 设备WIFI配置信息上报
#define MQTT_UP_MSG_TYPE_4 0x0004 // 设备iBeacon信息上报

#define MQTT_DOWN_MSG_TYPE_0 0x1000 // 控制设备信息上传
#define MQTT_DOWN_MSG_TYPE_1 0x1001 // 推送设备升级
#define MQTT_DOWN_MSG_TYPE_2 0x1002 // 推送设备WIFI配置
#define MQTT_DOWN_MSG_TYPE_4 0x1004 // 推送设备iBeacon配置

struct lgw_pkt_tx_s
{
    uint32_t    freq_hz;        /*!> center frequency of TX */
    uint8_t     tx_mode;        /*!> select on what event/time the TX is triggered */
    uint32_t    count_us;       /*!> timestamp or delay in microseconds for TX trigger */
    uint8_t     rf_chain;       /*!> through which RF chain will the packet be sent */
    int8_t      rf_power;       /*!> TX power, in dBm */
    uint8_t     modulation;     /*!> modulation to use for the packet */
    uint8_t     bandwidth;      /*!> modulation bandwidth (LoRa only) */
    uint32_t    datarate;       /*!> TX datarate (baudrate for FSK, SF for LoRa) */
    uint8_t     coderate;       /*!> error-correcting code of the packet (LoRa only) */
    bool        invert_pol;     /*!> invert signal polarity, for orthogonal downlinks (LoRa only) */
    uint8_t     f_dev;          /*!> frequency deviation, in kHz (FSK only) */
    uint16_t    preamble;       /*!> set the preamble length, 0 for default */
    bool        no_crc;         /*!> if true, do not send a CRC in the packet */
    bool        no_header;      /*!> if true, enable implicit header mode (LoRa), fixed length (FSK) */
    uint16_t    size;           /*!> payload size in bytes */
    uint8_t     payload[256];   /*!> buffer containing the payload */
};

#ifdef __cplusplus
}
#endif
#endif

