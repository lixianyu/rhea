/*
 * THE FOLLOWING FIRMWARE IS PROVIDED: (1) "AS IS" WITH NO WARRANTY; AND 
 * (2)TO ENABLE ACCESS TO CODING INFORMATION TO GUIDE AND FACILITATE CUSTOMER.
 * CONSEQUENTLY, SEMTECH SHALL NOT BE HELD LIABLE FOR ANY DIRECT, INDIRECT OR
 * CONSEQUENTIAL DAMAGES WITH RESPECT TO ANY CLAIMS ARISING FROM THE CONTENT
 * OF SUCH FIRMWARE AND/OR THE USE MADE BY CUSTOMERS OF THE CODING INFORMATION
 * CONTAINED HEREIN IN CONNECTION WITH THEIR PRODUCTS.
 * 
 * Copyright (C) SEMTECH S.A.
 */
/*! 
 * \file       sx1276-Hal.c
 * \brief      SX1276 Hardware Abstraction Layer
 *
 * \version    2.0.B2 
 * \date       Nov 21 2012
 * \author     Miguel Luis
 *
 * Last modified by Miguel Luis on Jun 19 2013
 */
#include <stdint.h>
#include <stdbool.h>
#include <string.h>
#include "driver/gpio.h"
#include "platform.h"
#include "driver/spi_master.h"
#include "esp_log.h"
#include "Dione_config.h"
#if defined( USE_SX1276_RADIO )

//#include "ioe.h"
//#include "spi.h"
#include "sx1276-Hal.h"

extern void dumpBytes(const uint8_t *data, size_t count);
extern spi_device_handle_t g_spi_handle;
static const char *TAG = "sx1276Hal";
spi_device_handle_t spi_handle;
static uint8_t tx_data_spi[128]; ///< SPI master TX buffer.
static uint8_t rx_data_spi[128]; ///< SPI master RX buffer.

/*!
 * SX1276 RESET I/O definitions
 */

/*!
 * SX1276 SPI NSS I/O definitions
 */

/*!
 * SX1276 DIO pins  I/O definitions
 */


void SX1276InitIo( void )
{
    // Configure NSS as output
    

    // Configure radio DIO as inputs

    // Configure DIO0
    
    // Configure DIO1
    
    // Configure DIO2
    
    // REAMARK: DIO3/4/5 configured are connected to IO expander

    // Configure DIO3 as input
    
    // Configure DIO4 as input
    
    // Configure DIO5 as input
    #if 0
    esp_err_t ret;
	spi_bus_config_t buscfg;
	spi_device_interface_config_t devcfg;
	memset(&buscfg, 0, sizeof(buscfg));
	memset(&devcfg, 0, sizeof(devcfg));
	buscfg.miso_io_num = RHEA_LORA_MISO;
	buscfg.mosi_io_num = RHEA_LORA_MOSI;
	buscfg.sclk_io_num = RHEA_LORA_CLK;
	buscfg.quadwp_io_num = -1;
	buscfg.quadhd_io_num = -1;

	devcfg.clock_speed_hz = 5000000;
	devcfg.command_bits = 0;
	devcfg.mode = 0;
	devcfg.spics_io_num = RHEA_LORA_SS;
	devcfg.queue_size = 1;
    // Initialize the SPI bus
    ret = spi_bus_initialize(HSPI_HOST, &buscfg, 1);
    if (ret == ESP_OK)
    {
		ESP_LOGI(TAG, "spi_bus_initialize sucess.");
    }
	else
	{
		ESP_LOGE(TAG, "spi_bus_initialize fail.");
	}
    // Attach the SX1278/1276 to the SPI bus
    ret = spi_bus_add_device(HSPI_HOST, &devcfg, &spi_handle);
    if (ret == ESP_OK)
    {
		ESP_LOGI(TAG, "spi_bus_add_device sucess.");
    }
	else
	{
		ESP_LOGI(TAG, "spi_bus_add_device fail.");
	}
    gpio_pad_select_gpio(RHEA_LORA_DIO0);
    gpio_set_direction(RHEA_LORA_DIO0, GPIO_MODE_INPUT);
#endif
#if 0
    gpio_pad_select_gpio(RHEA_LORA_DIO1);
    gpio_set_direction(RHEA_LORA_DIO1, GPIO_MODE_INPUT);

    gpio_pad_select_gpio(RHEA_LORA_DIO2);
    gpio_set_direction(RHEA_LORA_DIO2, GPIO_MODE_INPUT);

    gpio_pad_select_gpio(RHEA_LORA_DIO3);
    gpio_set_direction(RHEA_LORA_DIO3, GPIO_MODE_INPUT);
    //gpio_set_level(RHEA_LORA_RESET, 1);
#endif
}

void SX1276SetReset( uint8_t state )
{
    if( state == RADIO_RESET_ON )
    {
        // Set RESET pin to 0

        // Configure RESET as output
    }
    else
    {
        // Configure RESET as input
    }
}

void SX1276Write( uint8_t addr, uint8_t data )
{
    SX1276WriteBuffer( addr, &data, 1 );
}

void SX1276Read( uint8_t addr, uint8_t *data )
{
    SX1276ReadBuffer( addr, data, 1 );
}

void SX1276WriteBuffer( uint8_t addr, uint8_t *buffer, uint8_t size )
{
	//ESP_LOGE(TAG, "Enter %s", __func__);
    esp_err_t ret;
    struct spi_transaction_t tt;

    memset(&tt, 0, sizeof(spi_transaction_t));
    tt.length = 8 * (1 + size);                     //adr is 8 bits
    tx_data_spi[0] = addr | 0x80;
	memcpy(tx_data_spi + 1, buffer, size);
    tt.tx_buffer = tx_data_spi;
	tt.rx_buffer = rx_data_spi;
    ret = spi_device_transmit(spi_handle, &tt);  //Transmit!
    if (ret != ESP_OK)
    {
		ESP_LOGE(TAG, "SX1276WriteBuffer fail, ret=%d", ret);
    }
}

void SX1276ReadBuffer( uint8_t addr, uint8_t *buffer, uint8_t size )
{
	//ESP_LOGE(TAG, "Enter %s", __func__);
    esp_err_t ret;
    struct spi_transaction_t tt;

	memset(tx_data_spi, 0, sizeof(tx_data_spi));
	//memset(rx_data_spi, 0, sizeof(rx_data_spi));
    memset(&tt, 0, sizeof(spi_transaction_t));
    tt.length = 8 * (1 + size);                     //adr is 8 bits
    tt.rx_buffer = rx_data_spi;
	tx_data_spi[0] = addr;
	tt.tx_buffer = tx_data_spi;
    ret = spi_device_transmit(spi_handle, &tt);  //Transmit!
    if (ret == ESP_OK)
    {
		//ESP_LOGI(TAG, "SX1276ReadBuffer OK.");
		memcpy(buffer, rx_data_spi + 1, size);
    }
	else
	{
		ESP_LOGE(TAG, "SX1276ReadBuffer fail.");
	}
}

void SX1276WriteFifo( uint8_t *buffer, uint8_t size )
{
    SX1276WriteBuffer( 0, buffer, size );
}

void SX1276ReadFifo( uint8_t *buffer, uint8_t size )
{
    SX1276ReadBuffer( 0, buffer, size );
}

uint8_t SX1276ReadDio0( void )
{
    //return GPIO_ReadInputDataBit( DIO0_IOPORT, DIO0_PIN );
    return gpio_get_level(RHEA_LORA_DIO0);
}

uint8_t SX1276ReadDio1( void )
{
    //return GPIO_ReadInputDataBit( DIO1_IOPORT, DIO1_PIN );
    return gpio_get_level(RHEA_LORA_DIO1);
}

uint8_t SX1276ReadDio2( void )
{
    //return GPIO_ReadInputDataBit( DIO2_IOPORT, DIO2_PIN );
    return gpio_get_level(RHEA_LORA_DIO2);
}

uint8_t SX1276ReadDio3( void )
{
    //return IoePinGet( RF_DIO3_PIN );
    return gpio_get_level(RHEA_LORA_DIO3);
}

uint8_t SX1276ReadDio4( void )
{
    //return IoePinGet( RF_DIO4_PIN );
    return gpio_get_level(RHEA_LORA_DIO4);
}

uint8_t SX1276ReadDio5( void )
{
    //return IoePinGet( RF_DIO5_PIN );
    return gpio_get_level(RHEA_LORA_DIO5);
}

void SX1276WriteRxTx( uint8_t txEnable )
{
    #if 0
    if( txEnable != 0 )
    {
        IoePinOn( FEM_CTX_PIN );
        IoePinOff( FEM_CPS_PIN );
    }
    else
    {
        IoePinOff( FEM_CTX_PIN );
        IoePinOn( FEM_CPS_PIN );
    }
    #endif
}

#endif // USE_SX1276_RADIO
